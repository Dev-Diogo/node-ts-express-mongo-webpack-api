import * as config from 'config';
import * as jwt from 'jsonwebtoken';

const generateJWT = (payload: any) => {

  const secret: string = config.get('secret');
  const token: any = jwt.sign(payload, secret, {algorithm: 'HS512', expiresIn: '12h', issuer: 'TaskManager'});
  return token;
};

const decodeJWT = (payload: any) => {
  const token = payload.split(' ')[1];
  return jwt.verify(token, config.get('secret'));
};
export { generateJWT, decodeJWT };
