const errorHandler = (err, req, res, next) => {
  if (err.code === 'permission_denied') {
    res.status(403).jsonp({error: 'Request Denied not enough permissions'});
  }
  if (err.name === 'UnauthorizedError') {
    res.status(401).jsonp({error: 'Invalid Token'});
  }
};

export { errorHandler };
