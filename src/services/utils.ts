import { Requests } from '@/models';
import { encrypt } from '@/services/aesHandler';
import * as archiver from 'archiver';
import * as fs from 'fs';

const generatePassowrd = (length) => {
  let result = '';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const uploadFile = (Id: any, File: any) => {
  let timestamp: number = Math.floor(new Date().getTime() / 1000);
  let dir = `Uploads/${Id}`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  const timeName = `${timestamp}${File.name}`;
  let hw = encrypt(`${timestamp}${File.name}`);
  File.mv(`${dir}/${timeName}`);

  const file = {
    iv: hw.iv,
    data: hw.encryptedData,
    timeName,
    originalName: File.name,
    Date: new Date(),
  };
  Requests.updateOne({_id: Id}, {
    $push: {
      files: file,
    },
  }).exec().then().catch();
};
const zipFile = async (id, files) => {
  let dir = `Uploads/${id}/${id}.zip`;
  let output = fs.createWriteStream(dir);
  let archive = archiver('zip', {
    zlib: {level: 9},
  });
  output.on('close', () => {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
  });

  archive.pipe(output);
  await AppendFiles(archive, files, id).then(async () => {
    await archive.finalize();
  });

};

const AppendFiles = async (archive, files, id) => {
  await files.forEach((temp: any) => {
    archive.append(fs.createReadStream(`Uploads/${id}/${temp.timeName}`), {name: temp.originalName});
    console.log('added' + temp.originalName);
  });
};
export { generatePassowrd, uploadFile, zipFile };
