import { Team, User } from '@/models';
import * as _ from 'lodash';
import { decodeJWT, logger } from 'services';

const UserPermHandler = (token: any, permission: string, callback) => {

  const {_id}: any = decodeJWT(token);

  User.findById(_id).select('permissions is_active')

    .exec()

    .then((doc) => {

      const {permissions, is_active}: any = doc;
      logger.info('[Service User Permission Handler] cheking User : ' + doc._id);
      let flag = false;
      if (is_active) {
        for (const perm of permissions) {

          if (_.isEqual(perm, permission) || _.isEqual(perm, 'power:admin')) {

            flag = true;
            break;

          }
        }
      }

      callback(flag);

    })
    .catch((err) => {

      logger.error(`[Service User Permission Handler] Error: ${err}`);

    });

  return false;
};

const TeamPermHandler = (token: any, permission: string, teamId: string, callback) => {

  const {_id}: any = decodeJWT(token);
  let flag = false;

  Team.findOne({_id: teamId})

    .exec()

    .then((doc: any) => {

      doc.members.forEach((member) => {

        logger.error(`[Team Permissions Handler ] mid:${member.member_id}uid:${_id}is ${_.isEqual(member._id, _id)}`);
        flag = member.member_id.toString() === _id.toString();

      });

      if (flag) {

        callback(true);

      } else {

        callback(false);

      }

    })
    .catch((err) => {

      logger.error(`[Service Team Permission Handler] Error: ${err}`);

    });
};

const isUserActive = (Id, callback) => {
  User.findById({_id: Id}).select('is_active')

    .exec()

    .then((doc) => {

      const {is_active}: any = doc;

      callback(is_active);

    });
};
const DeniedResponse = (res) => {
  res.status(403).jsonp({error: ['Not Enough Permissions']});
};
export { UserPermHandler, TeamPermHandler, DeniedResponse };
