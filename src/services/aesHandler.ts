import * as crypto from 'crypto';

const algorithm = 'aes-256-cbc';
// const key = crypto.randomBytes(32);
const password = '?J4VJE!m@LZ?z*K$5ZuVEe=Sa^^-97nX_HnH4LpGGFWjSCsX+@U_n7?9^W^c=%2&w5RavgKr6K3vt6hx6BAj2=xKy?=-tLwK42d*=E@Tw*_EnDtf^jhU9HLXzc=P_Mk^64bT*L4#a-AX^gf+NpdA66tQ*@NhP7zK*9^9wp#WH-3V5HaMmaxYFE95hLdkAXWjT!Kh7aHp*&t!H$t^jWyXn+dHdDG97&KPqek7v7@3u4#aZ%mdrbfAZQA-JQq@3pw@X!NX@nSP-8*$MGn&bQUwV*%NDbvwkBttS#Mz5E4JP5_+%v&uBdBYwG3gXdsDw!DY?_kG8YPx?HB5ynBuc?R&Edze@mWyb2M4t@3VWL6mRAyZJhJS-MhJCZt&VEVuJYB8?+n-vXdagTUt4GrLQ8Y8@ZuqF9M?ymyf%Dy2k5k_Dg2Skvxz_Np8Ff==RvJUAEA=fsZufDfp!KevjW^#ex#LdqXYC_@rsMWBE?w=+F#_h2tkj==tdNVX-ss95BdPcYPcGw_B_N2hDmF?-wPQEHPp_6q3%@dGu6EfMJ6^MHuBp5jz_vj&Q?L-Z-Y_s$EsUV!u$9gV&M^33x-5QUSBUmsb_rVnJzNc%n-wP35U-Krc#!n!^f5G$EMZHFyf?dpEUD_ELQ78t-Zsgys4Bk7CmrCW%GvGXMsRw6-yY3+RuK$LqQp&jLQu@9vjtNR@h7w9e&s?2r6FMtpAh*Qu_t6Pn+LH8U%wjLh9pLsj=g4!TuJ8P=%ZF6?MnwE3c?mKYVp3%j5%ZXhDh&BuDS&STu=-bL8pv*B7x=JppbF7tT$Hg$-QX@m**?+vXRMm*g@q6Z_TWeDsTkLM7-2sY@DZg8?@nwABYSw!VLJ2A_Ckw4?WNxFYJyFhUC5G9z4#*FfkckdR3qy4+KK9zJN-tLF%&uudr!sLWKV_y=a=7pW@+x$yPmD!ANHnzJEtdQZZ3&XstfpK@bQcubK^%f#C$XDkwVT!Stth7b8A_MNHWcyCeW3+cW%DBnQW8vfF6pSMf_9$dRKvgJCh8MG%W6@jhW*hA?WgG4!%yxcYP@+qH3^@5wJMdEyKCMtfDcmw723w!jw-vXBsGtkqaLcrb&#f8jTfSGBA#e9M_zV5zD%FFEVEP6aCw-cY$e7EWkUDqxF6$#Y83WSaQGpxQrLb7X2@4UcPa3$4UY9DfMEWktD73ebv_=nkbhgB3_c%A48$AQygg+Stc_%e-SS67bTE8RnGP!3&VZ^D2&yf!*2!KkEyX=z4gM&KqzSpP=&Upq4b3URZC&GNDMgB*3krH_FsxqCqg96-c-Y$^dk5QT6UfaPzhh#xCN%ExLnsgGv9=469-4rg$_GdNBLJ=HYr^Uw*&EFTw236dN5@Dshd?zr&F?RtAEqwXR^S=@2=Z+UV&c?Bk+F6R98h$XXp_nX??!ZD6VFXf7uCN6&5BgrntBRsw*-s4UQrtYrbZ9yKhEc_##Yx4SF^Qy9g&9VYUUpthTFdF^u-*umM4@b3CaCPefjV64yYZTAbcLQH=yXUGMRcbvQ=2AwQZ2fBAH-QhgyJh6bbE4$kQxW+ErHK4Uz6^U2A!x#z-RMjzDxwyvJzhe_SAGCnPMkP&QT_=W=gvKQz5QJJnXeu@S7Eq*Kz8RB$Qdt775EhzB*m8mJRNJ*Qw#euxV@TqZ$uZa9ZhfnRg*fYe6evuV4=9F--k6asPRz+fmk7AgvSx*gXQ*KV$gr*XG!!xh*MUrCbV*kx$=6nnt-%X6F-sY*22SG-tPRWT-xqe2wKez73b#n9pp9t!cQ8!-V46VFR&@PHpN=WS!b8g4CTLw-K2W-XVjvQVk3T_DemRA+KHx!qyyxgJGuGNfbD4mT&vW8JKL7Pas$=96a*CgW6aJ3_Cawup*YbS#^XCTSZeuBzpH568+tunWxVhJULC*y+6n7w7ABQ@sAnUzLtperSGTcKYX%redG3qs-E+EzvzEWM$n!Mx^bgJ@qq+BASj5cUeS!yXWes#SNJ$^65!Gz#&k7xYaX=RKxc=NYk%bXEPbb_EKjnX*F7A*8xVT@RBJ7KxeS?F8t$G#_*U%5_tdXh';
const key = crypto.scryptSync(password, 'salt', 32);
const iv = crypto.randomBytes(16);

const encrypt = (text) => {
  let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return {iv: iv.toString('hex'), encryptedData: encrypted.toString('hex')};
};

const decrypt = (text) => {
  let iv = Buffer.from(text.iv, 'hex');
  let encryptedText = Buffer.from(text.encryptedData, 'hex');
  let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
};
export { encrypt, decrypt };
