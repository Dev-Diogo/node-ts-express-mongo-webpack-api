import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as config from 'config';
import * as cors from 'cors';
import * as express from 'express';
import * as fileUpload from 'express-fileupload';
import * as jwt from 'express-jwt';
import * as expressStatusMonitor from 'express-status-monitor';
import * as helmet from 'helmet';
import * as methodOverride from 'method-override';
import * as morgan from 'morgan';
import * as path from 'path';
import { ApiRoutes } from './routes';
import { errorHandler, logger } from './services';

export class Server {

  constructor() {
    // create expresses application
    this.app = express();

    // configure application
    this.config();

    // add routes
    this.routes();
  }

  public app: express.Application;

  public static bootstrap(): Server {
    return new Server();
  }

  public config() {

    // add static paths
    this.app.use(express.static(path.join(__dirname, 'public')));

    this.app.use(jwt({secret: config.get('secret')}).unless({
      path: ['/api/versions/v1/auth/login', '/', '/api',
        {url: '*', methods: ['OPTIONS']},
      ],

    }));

    // mount logger
    this.app.use(morgan('tiny', {
      stream: {
        write: (message: string) => logger.info(message.trim()),
      },
    } as morgan.Options));

    // mount json form parser
    this.app.use(bodyParser.json({limit: '50mb'}));

    // mount query string parser
    this.app.use(bodyParser.urlencoded({

      extended: true,

    }));

    this.app.use(helmet());
    this.app.use(fileUpload());
    this.app.use(cors());
    this.app.use(compression());
    this.app.use(methodOverride());
    this.app.use(expressStatusMonitor());
    this.app.use(errorHandler);

  }

  private routes() {

    // use router middleware
    this.app.use(ApiRoutes.path, ApiRoutes.router);

  }

}
