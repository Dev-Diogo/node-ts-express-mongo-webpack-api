import * as config from 'config';
import * as fs from "fs";
import * as https from "https";
import * as mongoose from 'mongoose';
import { Server } from './app';
import { logger } from './services';

// create http server
let env = process.env.NODE_ENV;
const port = Number(config.get('port'));
const hostname = '0.0.0.0';
let key = fs.readFileSync('Encryption/private.key');
let cert = fs.readFileSync('Encryption/certificate.crt');
let ca = fs.readFileSync('Encryption/ca_bundle.crt');
let options = {
  key,
  cert,
  ca,
};
const app = Server.bootstrap().app;
let server: any;
switch (env) {
  case 'development':
    server = app.listen(port, hostname, () => {
      logger.info('[-------[ Server Fully Started ]-------]');
    });
    break;
  case 'production':
    server = https.createServer(options, app).listen(port, hostname, () => {
      logger.info('[-------[ Server Fully Started ]-------]');
    });
    break;
  default:

    break;
}
// log server start and port
logger.info(`[Server Start] Started at Port: ${port}`);

// MongoDb Connection
mongoose.connect(
  'mongodb+srv://Admin:Portugues174_@cluster0-gkdca.gcp.mongodb.net/TaskManager?retryWrites=true',
  {useNewUrlParser: true},
).then(() => {

// log connection  to mongoDb Successfully
  logger.info(`[MongoDD Connected] Connected Successfully`);

});
export { server, app };
