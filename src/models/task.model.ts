import { Requests } from '@/models/request.model';
import * as mongoose from 'mongoose';

const taskSchema = new mongoose.Schema({

  _id: {type: mongoose.Schema.Types.ObjectId, required: true},

  name: {type: String, required: true},

  description: {type: String, required: true},

  created_at: {type: mongoose.Schema.Types.Date, required: true},

  modified_at: {type: mongoose.Schema.Types.Date, required: false},

  closed_at: {type: mongoose.Schema.Types.Date, required: false},

  deadline: {type: mongoose.Schema.Types.Date, required: true},

  isCancelled: {type: mongoose.Schema.Types.Boolean, required: false},

  cancelled_reason: {type: String, required: false},

  status_id: {type: mongoose.Schema.Types.ObjectId, required: true},

  technician_id: {type: mongoose.Schema.Types.ObjectId, required: true},

  request_id: {type: mongoose.Schema.Types.ObjectId, required: true, ref: Requests},

  dependencies: {type: [mongoose.Schema.Types.ObjectId], required: true},

  sub_category_id: {type: mongoose.Schema.Types.ObjectId, required: true},

});

interface ITask {
  _id?: mongoose.Schema.Types.ObjectId;

  name?: string;

  description?: string;

  created_at?: mongoose.Schema.Types.Date;

  modified_at?: mongoose.Schema.Types.Date;

  closed_at?: mongoose.Schema.Types.Date;

  deadline?: mongoose.Schema.Types.Date;

  isCancelled?: mongoose.Schema.Types.Boolean;

  cancelled_reason?: string;

  status_id?: mongoose.Schema.Types.ObjectId;

  priority_id?: mongoose.Schema.Types.ObjectId;

  category_id?: mongoose.Schema.Types.ObjectId;

  technician_id?: mongoose.Schema.Types.ObjectId;

  request_id?: mongoose.Schema.Types.ObjectId;

  dependencies?: mongoose.Schema.Types.ObjectId[];

  sub_category_id?: mongoose.Schema.Types.ObjectId[];

}

const Task = mongoose.model('Task', taskSchema);

export { Task, ITask };
