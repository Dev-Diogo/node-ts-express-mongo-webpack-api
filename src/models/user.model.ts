import * as mongoose from 'mongoose';

const userSchema = new mongoose.Schema({

  _id: mongoose.Schema.Types.ObjectId,

  internal_code: {type: String, required: false},

  email: {type: String, required: true},

  first_name: {type: String, required: true},

  last_name: {type: String, required: true},

  email_verified: {type: mongoose.Schema.Types.Boolean, required: true, select: false},

  password: {type: String, select: false, required: true},

  type: {type: String, select: false, required: true},

  permissions: {type: [String], required: true, select: false},

  is_active: {type: mongoose.Schema.Types.Boolean, required: true},

  created_at: {type: mongoose.Schema.Types.Date, required: true},

  category: {type: [String], required: false, select: false},

  technician_id: {type: mongoose.Schema.Types.ObjectId, required: false},

  __v: {type: Number, select: false},

});

interface IUser {

  _id?: mongoose.Schema.Types.ObjectId;

  email?: string;

  first_name?: string;

  last_name?: string;

  email_verified?: boolean;

  password?: string;

  permissions?: string[];

  created_at?: mongoose.Schema.Types.Date;

  type?: string;

  category?: string[];

  internal_code?: string;

}

const User = mongoose.model('User', userSchema);

export { User, IUser };
