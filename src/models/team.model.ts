import { User } from '@/models/user.model';
import * as mongoose from 'mongoose';

const teamSchema = new mongoose.Schema({

  _id: {type: mongoose.Schema.Types.ObjectId, required: true},

  team_name: {type: String, required: true},

  owner_id: {type: mongoose.Schema.Types.ObjectId, required: true, ref: User},

  creation_date: {type: mongoose.Schema.Types.Date, required: true},

  members: [{

    member_id: {type: mongoose.Schema.Types.ObjectId, required: true, ref: User},

    join_date: {type: mongoose.Schema.Types.Date, required: true},

    team_permissions: {type: [String], required: true},

  }],

  __v: {type: Number, select: false},

});

interface ITeam {
  _id?: mongoose.Schema.Types.ObjectId;

  team_name?: string;

  owner_id?: string;

  creation_date?: mongoose.Schema.Types.Date;

  members?: [{

    member_id?: mongoose.Schema.Types.ObjectId;

    join_date?: mongoose.Schema.Types.Date;

    team_permissions?: string[];

  }];

}

const Team = mongoose.model('Team', teamSchema);

export { Team, ITeam };
