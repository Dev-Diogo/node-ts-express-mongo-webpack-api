import * as mongoose from 'mongoose';

const requestLogsSchema = new mongoose.Schema({

  _id: {type: mongoose.Schema.Types.ObjectId, required: true},

  state: {type: String, required: true},

  date: {type: String, required: true},

  description: {type: String, required: true},

  client_id: {type: mongoose.Schema.Types.ObjectId, required: true},

  creator_id: {type: mongoose.Schema.Types.ObjectId, required: true},
});

const RequestLogs = mongoose.model('RequestLogs', requestLogsSchema);

export { RequestLogs };
