import * as mongoose from 'mongoose';

const documentSchema = new mongoose.Schema({

  _id: {type: mongoose.Schema.Types.ObjectId, required: true},

  name: {type: String, required: true},

  category_id: {type: mongoose.Schema.Types.ObjectId, required: false},

  client_id: {type: mongoose.Schema.Types.ObjectId, required: true},

  validated: {type: mongoose.Schema.Types.Boolean, required: true},

  validated_by: {type: mongoose.Schema.Types.ObjectId, required: false},

  validated_at: {type: mongoose.Schema.Types.Date, required: false},

  created_at: {type: mongoose.Schema.Types.Date, required: true},

  modified_at: {type: mongoose.Schema.Types.Date, required: false},

  rejected: {type: mongoose.Schema.Types.Boolean, required: false},

});

interface IDocument {

  _id?: mongoose.Schema.Types.ObjectId;

  name?: string;

  category_id?: mongoose.Schema.Types.ObjectId;

  client_id?: mongoose.Schema.Types.ObjectId;

  validated?: mongoose.Schema.Types.Boolean;

  validated_by?: mongoose.Schema.Types.ObjectId;

  validated_at?: mongoose.Schema.Types.Date;

  created_at?: mongoose.Schema.Types.Date;

  modified_at?: mongoose.Schema.Types.Date;

  rejected?: mongoose.Schema.Types.Boolean;

}

const Request = mongoose.model('Request', documentSchema);

export { Request, IDocument };
