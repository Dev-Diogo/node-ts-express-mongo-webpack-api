import { Category } from '@/models/category.model';
import { Team } from '@/models/team.model';
import { User } from '@/models/user.model';
import * as mongoose from 'mongoose';

const requestSchema = new mongoose.Schema({

  _id: {type: mongoose.Schema.Types.ObjectId, required: true},

  name: {type: String, required: true},

  client_id: {type: mongoose.Schema.Types.ObjectId, required: true, ref: User},

  creator_id: {type: mongoose.Schema.Types.ObjectId, required: true, ref: User},

  description: {type: String, required: true},

  created_at: {type: mongoose.Schema.Types.Date, required: true},

  modified_at: {type: mongoose.Schema.Types.Date, required: false},

  closed_at: {type: mongoose.Schema.Types.Date, required: false},

  limit_date: {type: mongoose.Schema.Types.Date, required: false},

  create_date: {type: mongoose.Schema.Types.Date, required: false},

  is_cancelled: {type: mongoose.Schema.Types.Boolean, required: false},

  cancelled_reason: {type: String, required: false},

  status: {type: Boolean, required: true},

  criticity: {type: String, required: false},

  team_id: {type: mongoose.Schema.Types.ObjectId, required: false, ref: Team},

  category_id: {type: mongoose.Schema.Types.ObjectId, required: false, ref: Category},

  obs: {
    type: [{
      _id: String,
      creator_id: {type: mongoose.Schema.Types.ObjectId, required: false, ref: User},
      text: String,
      created_at: {type: mongoose.Schema.Types.Date, required: false},
    }], required: false,
  },

  files: {
    type: [{
      iv: String,
      data: String,
      timeName: String,
      originalName: String,
      Date,

    }], required: true,
  },
});

interface IRequest {

  _id?: mongoose.Schema.Types.ObjectId;

  name?: string;

  description?: string;

  created_at?: mongoose.Schema.Types.Date;

  modified_at?: mongoose.Schema.Types.Date;

  closed_at?: mongoose.Schema.Types.Date;

  deadline?: mongoose.Schema.Types.Date;

  is_cancelled?: mongoose.Schema.Types.Boolean;

  cancelled_reason?: string;

  status_id?: mongoose.Schema.Types.ObjectId;

  criticity?: string;

  category_id?: mongoose.Schema.Types.ObjectId;

}

const Requests = mongoose.model('Request', requestSchema);

export { Requests, IRequest };
