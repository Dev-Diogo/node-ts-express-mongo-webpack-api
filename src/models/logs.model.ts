import * as mongoose from 'mongoose';

const logsSchema = new mongoose.Schema({

  _id: {type: mongoose.Schema.Types.ObjectId, required: true},

  type: {type: String, required: true},

  date: {type: String, required: true}

});

const Logs = mongoose.model('Logs', logsSchema);

export { Logs };
