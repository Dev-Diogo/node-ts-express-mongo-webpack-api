import * as mongoose from 'mongoose';

const categorySchema = new mongoose.Schema({

  _id: {type: mongoose.Schema.Types.ObjectId, required: true},

  name: {type: String, required: true},

  content: {type: String, required: true},

  is_active: {type: mongoose.Schema.Types.Boolean, required: true},

  sub_category: [{

    _id: {type: mongoose.Schema.Types.ObjectId, required: false},

    name: {type: String, required: false},

    content: {type: String, required: false},

    is_active: {type: Boolean, required: false},

    type: {type: String, required: false},

  }],

});

interface ICategory {
  name?: string;

  description?: string;

  sub_category?: [{

    _id?: mongoose.Schema.Types.ObjectId,

    name?: string,

    description?: string;

  }];

}

const Category = mongoose.model('Category', categorySchema);

export { Category, ICategory };
