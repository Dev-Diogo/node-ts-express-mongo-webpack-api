import { IUser, User } from '@/models';
import { errorHandler, generateJWT, generatePassowrd, logger } from '@/services';
import { UserPermHandler } from '@/services/permissionsHandler';
import * as bcrypt from 'bcryptjs';
import { NextFunction, Request, Response } from 'express';
import { check, validationResult } from 'express-validator/check';
import * as mongoose from 'mongoose';
import { BaseRoute } from '../../../route';

export class AuthRoute extends BaseRoute {
  public static path = '/auth';
  private static instance: AuthRoute;

  private constructor() {

    super();

    this.init();
    this.router.use(errorHandler);
  }

  static get router() {
    if (!AuthRoute.instance) {
      AuthRoute.instance = new AuthRoute();
    }
    return AuthRoute.instance.router;
  }

// [ROUTER}
  private init() {
    // log
    logger.info('[AuthRoute] Creating Auth routes.');

    // login endpoint
    this.router.post('/login', [

      // username must be an email
      check('email').isEmail(),

      // password must be present
      check('password').exists(),

    ], this.login);

    // register endpoint
    this.router.post('/register', [

      // username must be an email
      check('email').isEmail(),

      check('first_name').isString(),

      check('last_name').isString(),

      check('created_at').exists(),

    ], this.register);

    this.router.options('/register', this.options);

    this.router.options('/login', this.options);
  }

  private register(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'users:create', (result) => {
      const errors = validationResult(req);

      // check if parameters are missing
      if (!errors.isEmpty()) {
        logger.info({errors: errors.array()});
        return res.status(422).json({errors: errors.array()});

      }

      const {email, first_name, last_name, created_at, type, technician_id} = req.body;

      User.findOne({email})

        .exec()

        .then((doc) => {

          if (doc) {

            res.status(403).jsonp({errors: ['Duplicate Email']});

          } else {

            const newPassword = generatePassowrd(16);
            console.log(`New Password : ${newPassword}`);
            const hashed = bcrypt.hashSync(newPassword, 12);
            const isActive = true;
            let permissions = [];
            let internalCode;
            let categoryId;
            if (type === 'client') {
              internalCode = req.body.internal_code;
              permissions = [
                'request:create',
                'request:read',
                'request:update',
                'category:read',
              ];
              // tslint:disable-next-line:align
            } else if (type === 'technician') {
              categoryId = req.body.category_id;
              permissions = [
                'request:create',
                'request:read',
                'request:update',
                'client:read',
                'category:read'
              ];
            }
            const user: any = new User({

              _id: new mongoose.Types.ObjectId(),
              email,
              password: hashed,
              permissions,
              first_name,
              last_name,
              email_verified: false,
              created_at,
              is_active: isActive,
              type,
              internal_code: internalCode,
              category_id: mongoose.Types.ObjectId(categoryId),

            });
            if (type === 'client' && technician_id !== 'null') {
              user.technician_id = mongoose.Types.ObjectId(technician_id);
            }

            // if (type === 'client' && technician_id !== "null") {
            //   user.technician_id = technician_id;
            // }

            user
              .save()

              .then(() => {

                res.status(200).jsonp(newPassword);

              })

              .catch((err) => {

                logger.error(err);

                res.status(500).jsonp({errors: err});

              });

          }
        })
        .catch((err) => {
          res.status(500).jsonp({errors: err});
        });
    });
  }

  private login(req: Request, res: Response, next: NextFunction) {

    const errors = validationResult(req);

    // check if parameters are missing
    if (!errors.isEmpty()) {

      return res.status(422).json({errors: errors.array()});

    }

    const {email, password: pass} = req.body;

    User.findOne({email}).select('+password')

      .exec()

      .then((doc) => {

        if (doc) {

          const {_id, password, type, is_active} = doc.toJSON();

          const isTrue = bcrypt.compareSync(pass, password);

          if (isTrue && is_active) {

            const payload: IUser = {

              _id,

            };

            const token = {
              token: generateJWT(payload),
            };

            res.status(200).jsonp(token);

          } else if (!isTrue) {

            res.status(500).jsonp({errors: ['invalid login information']});

          } else if (!is_active) {
            res.status(500).jsonp({errors: ['Account has been locked']});
          }
        } else {
          res.status(500).jsonp({errors: ['invalid login information']});
        }

      })
      .catch((err) => {
        logger.error('[Mongoose Login]' + err);
      });
  }

  private options(req: Request, res: Response, next: NextFunction) {

    res.status(200).jsonp({message: 'OK'});

  }
}
