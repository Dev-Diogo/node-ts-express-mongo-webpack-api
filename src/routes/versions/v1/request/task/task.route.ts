import { BaseRoute } from '@/routes/route';
import { logger } from '@/services';
import { NextFunction, Request, Response } from 'express';

export class TaskRoute extends BaseRoute {
  public static path = '/v1';
  private static instance: TaskRoute;

  private constructor () {

    super();
    this.init();

  }

  static get router () {

    if (!TaskRoute.instance) {

      TaskRoute.instance = new TaskRoute();

    }
    return TaskRoute.instance.router;
  }

  private init () {
    // log
    logger.info('[TaskRoute] Creating Task routes.');

    // add index page route
    this.router.get('/', this.get);

  }

  private async get (req: Request, res: Response, next: NextFunction) {

    res.json('request');

  }
}
