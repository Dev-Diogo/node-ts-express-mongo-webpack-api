import { Requests, User } from '@/models';
import { BaseRoute } from '@/routes/route';
import { decodeJWT, logger, uploadFile, zipFile } from '@/services';
import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator/check';
import * as mongoose from 'mongoose';

export class RequestRoute extends BaseRoute {
  public static path = '/requests';
  private static instance: RequestRoute;

  private constructor() {

    super();
    this.init();

  }

  static get router() {

    if (!RequestRoute.instance) {

      RequestRoute.instance = new RequestRoute();

    }

    return RequestRoute.instance.router;
  }

  private init() {
    // log
    logger.info('[RequestRoute] Creating Request routes.');

    // add index page route
    this.router.get('/', this.get);
    this.router.get('/me', this.getMe);
    this.router.get('/download/all/:ID', this.getAllFiles);
    this.router.get('/download/:ID/:DocID', this.getFile);

    this.router.get('/:ID', this.getByID);

    this.router.post('/', this.post);

    this.router.patch('/:IDRequest', this.patch);
    this.router.delete('/:IDRequest', this.delete);

  }

  private async get(req: Request, res: Response, next: NextFunction) {
    const TOKEN = req.headers.authorization;
    const token: any = decodeJWT(TOKEN);
    let requests: any;
    Requests.find().populate('client_id creator_id team_id category_id ')
      .exec()
      .then((docs) => {

        res.status(200).jsonp({docs});

      })
      .catch((err) => {

        logger.error('[Request Route GET]' + err);

      });
  }

  private async getMe(req: Request, res: Response, next: NextFunction) {
    const TOKEN = req.headers.authorization;
    const token: any = decodeJWT(TOKEN);
    let requests: any;
    Requests.find({
      $or: [
        {creator_id: token._id}, {client_id: token._id},
      ],
    }).populate('client_id creator_id team_id category_id')
      .exec()
      .then((docs) => {

        res.status(200).jsonp({docs});

      })
      .catch((err) => {

        logger.error('[Request Route GETME]' + err);

      });
  }

  private async getByID(req: Request, res: Response, next: NextFunction) {

    Requests.findById(req.params.ID).populate('client_id creator_id team_id category_id obs.creator_id').exec()
      .then((result) => {
        res.status(200).jsonp(result);
      }).catch((err) => {
      logger.error('[Request Route GETBYID] ' + err);
      res.status(500).jsonp({error: ['could not process request']});
    });

  }

  private async getFile(req: Request, res: Response, next: NextFunction) {

    Requests.findOne({_id: req.params.ID}).exec().then((doc: any) => {
      const files = doc.files;

      let file: any;
      files.forEach((temp: any) => {
        console.log(temp);
        if (temp._id.toString() === req.params.DocID) {
          file = temp;
        }

      });
      if (file !== undefined) {
        const download = `Uploads/${doc._id}/${file.timeName}`;
        zipFile(doc._id, files);
        res.download(download); // Set disposition and send it.
      }

    }).catch();

  }

  private async getAllFiles(req: Request, res: Response, next: NextFunction) {

    Requests.findOne({_id: req.params.ID}).exec().then((doc: any) => {
      const files = doc.files;
      zipFile(doc._id, files).then(() => {
        console.log(true);
        res.download(`Uploads/${doc._id}/${doc._id}.zip`); // Set disposition and send it.
      });

    }).catch();

  }

  private async post(req: Request, res: Response, next: NextFunction) {
    console.log(req.body);
    const token: any = decodeJWT(req.headers.authorization);
    const errors = validationResult(req);

    // check if parameters are missing
    if (!errors.isEmpty()) {

      logger.info({errors: errors.array()});
      return res.status(422).json({errors: errors.array()});
    }
    try {

      let {request_name, start_date, technician_id, client_id, description, category_id, criticity, team_id, deadline, limit_date, creator_id} = req.body;
      const Files: any = req.files;

      let request: any;
      let length = 0;

      if (req.files !== null) {
        length = Object.keys(req.files['files[]']).length;

        if (req.files['files[]'].name !== undefined) {
          length = 1;
        }
      }
      User.findById(token._id).select('type').exec().then((user: any) => {
        creator_id = mongoose.Types.ObjectId(creator_id);
        client_id = mongoose.Types.ObjectId(client_id);
        if (user.type === 'client') {

          request = new Requests({
            _id: new mongoose.Types.ObjectId(),
            name: request_name,
            client_id: token._id,
            description,

            creator_id: token._id,
            created_at: new Date(),
            start_date,
            status: true,
            files: [],
            technician_id,
          });
          if (category_id !== 'null') {

            request.category_id = category_id;
          }

        } else {

          request = new Requests({
            _id: new mongoose.Types.ObjectId(),
            name: request_name,
            client_id,
            description,
            criticity,

            creator_id,
            created_at: new Date(),
            start_date,
            status: true,
            files: [],
            technician_id,
          })
          ;
          if (category_id !== 'null') {

            request.category_id = category_id;
          }

        }
        if (limit_date !== 'null') {

          request.limit_date = limit_date;
        }
        request
          .save()
          .then((doc) => {
            // res.status(200).jsonp({message: 'OK', doc});
            if (length === 1) {

              uploadFile(doc._id, req.files['files[]']);

            } else if (length > 1) {

              for (let i = 0; i < length; i++) {

                uploadFile(doc._id, Files['files[]'][i]);
              }
            }
            res.status(200).jsonp({doc});
          })
          .catch((err) => {
            logger.error(`[Error Request Save]${err}`);
            res.status(500).jsonp({error: err});

          });
      }).catch();

    } catch (e) {

      logger.error('[Upload File] error' + e);
      res.status(500).jsonp({errors: ['Failed to Process Request', e]});

    }

  }

  private async patch(req: Request, res: Response, next: NextFunction) {

    const token = req.headers.authorization;

    const {_id}: any = decodeJWT(token);

    let updateOps: any;

    for (const ops of req.body) {

      if (ops.propname === 'obs') {

        if (ops.action === 'add') {
          const obs = {
            _id: new mongoose.Types.ObjectId(),
            creator_id: mongoose.Types.ObjectId(_id),
            text: ops.value,
            created_at: new Date(),
          };
          Requests.updateOne({_id: req.params.IDRequest}, {
            $push: {obs},
          }).exec().then(() => {
            res.status(200).jsonp('OK');
          }).catch();
        } else if (ops.action === 'remove') {
          Requests.updateOne({_id: req.params.IDRequest}, {
            $pull: {
              obs: {
                _id: mongoose.Types.ObjectId(ops.value),
              },
            },
          }).exec().then(() => {
            res.status(200).jsonp('OK');
          }).catch();
        }
      } else {

        res.status(500).jsonp({error: 'illegal modification'});

      }

    }


  }

  private async put(req: Request, res: Response, next: NextFunction) {
    res.json('request');
  }

  private async delete(req: Request, res: Response, next: NextFunction) {

    res.json('request');

  }
}
