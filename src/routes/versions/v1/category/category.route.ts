import { Category } from '@/models/category.model';
import { logger } from '@/services';
import { DeniedResponse, UserPermHandler } from '@/services/permissionsHandler';
import { NextFunction, Request, Response } from 'express';
import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import { BaseRoute } from '../../../route';

export class CategoryRoute extends BaseRoute {
  public static path = '/categories';
  private static instance: CategoryRoute;
  w;

  private constructor() {

    super();
    this.init();

  }

  static get router() {

    if (!CategoryRoute.instance) {

      CategoryRoute.instance = new CategoryRoute();

    }

    return CategoryRoute.instance.router;
  }

  private init() {
    // log
    logger.info('[CategoryRoute] Creating Category routes.');

    // add index page route
    this.router.get('/', this.get);

    this.router.get('/all', this.getAll);

    this.router.get('/:ID', this.getByID);

    this.router.post('/:ID', this.post);
    this.router.post('/', this.post);

  }

  private async get(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'request:create', (result) => {
      if (!result) {
        DeniedResponse(res);
      }
      Category.find({is_active: true}).exec().then(
        (doc) => {
          res.status(200).jsonp({doc});
        },
      ).catch((err) => {
        res.status(500).jsonp({error: err});
      });

    });
  }

  private async getAll(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'category:read', (result) => {
      if (!result) {
        DeniedResponse(res);
      }

      Category.find({is_active: true}).exec().then(
        (doc) => {
          res.status(200).jsonp({doc});
        },
      ).catch((err) => {
        res.status(500).jsonp({error: err});
      });

    });
  }

  private async getByID(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'category:read', (result) => {
      if (!result) {
        DeniedResponse(res);
      }

      Category.find({_id: req.params.ID}).exec().then(
        (doc) => {
          res.status(200).jsonp({doc});
        },
      ).catch((err) => {
        res.status(500).jsonp({error: err});
      });

    });
  }

  private async post(req: Request, res: Response, next: NextFunction) {

    UserPermHandler(req.headers.authorization, 'category:read', (result) => {
      if (!result) {
        DeniedResponse(res);
      }
      const {category_name, sub_category_name, description, creator_id, documents, type} = req.body;
      const removeItem = [null];
      let FilesArray;
      if (documents !== undefined) {
        FilesArray = documents.filter((element) => element != null);
      }
      const category = new Category({
        _id: new mongoose.Types.ObjectId(),
        name: _.capitalize(category_name),
        content: description,
        creator_id,
        is_active: true,
        sub_category: {
          _id: new mongoose.Types.ObjectId(),
          name: _.capitalize(sub_category_name),
          files: FilesArray,
          type: type ? _.capitalize(type) : '',
        },
      });
      category.save().then((doc) => {
        res.status(200).jsonp({doc});
      }).catch((err) => {
        logger.error('[Category Route]' + err);
      });

    });
  }
}
