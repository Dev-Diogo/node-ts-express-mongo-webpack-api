import { CategoryRoute } from '@/routes/versions/v1/category/category.route';
import { ClientRoute } from '@/routes/versions/v1/client';
import { RequestRoute } from '@/routes/versions/v1/request';
import { TechnicianRoute } from '@/routes/versions/v1/technician';
import { logger } from '@/services';
import { NextFunction, Request, Response } from 'express';
import { BaseRoute } from '../../route';
import { AuthRoute } from './auth';
import { TeamsRoute } from './teams';
import { UsersRoute } from './users';

export class V1Route extends BaseRoute {
  public static path = '/v1';
  private static instance: V1Route;

  private constructor () {
    super();
    this.init();
  }

  static get router () {
    if (!V1Route.instance) {
      V1Route.instance = new V1Route();
    }
    return V1Route.instance.router;
  }

  private init () {
    // log
    logger.info('[V1Route] Creating V1 routes.');

    // add index page route
    this.router.get('/', this.get);
    this.router.use(UsersRoute.path, UsersRoute.router);
    this.router.use(AuthRoute.path, AuthRoute.router);
    this.router.use(TeamsRoute.path, TeamsRoute.router);
    this.router.use(ClientRoute.path, ClientRoute.router);
    this.router.use(CategoryRoute.path, CategoryRoute.router);
    this.router.use(RequestRoute.path, RequestRoute.router);
    this.router.use(TechnicianRoute.path, TechnicianRoute.router);
  }

  private async get (req: Request, res: Response, next: NextFunction) {
    res.json('v1');
    next();
  }
}
