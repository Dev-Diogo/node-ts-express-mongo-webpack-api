import { User } from '@/models';
import { decodeJWT, logger } from '@/services';
import { UserPermHandler } from '@/services/permissionsHandler';
import { NextFunction, Request, Response } from 'express';
import * as mongoose from 'mongoose';
import { BaseRoute } from '../../../route';

export class ClientRoute extends BaseRoute {
  public static path = '/clients';
  private static instance: ClientRoute;

  private constructor () {

    super();
    this.init();

  }

  static get router () {

    if (!ClientRoute.instance) {

      ClientRoute.instance = new ClientRoute();

    }

    return ClientRoute.instance.router;
  }

  private init () {
    // log
    logger.info('[ClientRoute] Creating Client routes.');

    // add index page route
    this.router.get('/', this.get);
    this.router.get('/me', this.getMe);

  }

  private async get (req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'client:read', (result) => {

      if (!result) {

        res.status(403).jsonp({message: 'Access Denied. Not enough permissions'});

      } else {
        User.find({type: 'client'}).exec().then(
          (doc) => {

            res.status(200).jsonp({doc});
          },
        ).catch((err) => {
          res.status(500).jsonp({error: err});
        });
      }

    });
  }

  private async getMe(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'client:read', (result) => {
      const token: any = decodeJWT(req.headers.authorization);
      if (!result) {

        res.status(403).jsonp({message: 'Access Denied. Not enough permissions'});

      } else {
        User.find({type: 'client', technician_id: mongoose.Types.ObjectId(token._id)}).exec().then(
          (doc) => {
            console.log({doc});
            res.status(200).jsonp({doc});
          },
        ).catch((err) => {
          res.status(500).jsonp({error: err});
        });
      }

    });
  }
}
