import { User } from '@/models';
import { decodeJWT, errorHandler, logger } from '@/services';
import { NextFunction, Request, Response } from 'express';
import { BaseRoute } from '../../../route';
import _ = require('lodash');

export class TechnicianRoute extends BaseRoute {
  public static path = '/technicians';
  private static instance: TechnicianRoute;

  private constructor () {
    super();

    this.init();
    this.router.use(errorHandler);
  }

  static get router () {
    if (!TechnicianRoute.instance) {
      TechnicianRoute.instance = new TechnicianRoute();
    }
    return TechnicianRoute.instance.router;
  }

  private init () {

    // log
    logger.info('[UsersRoute] Creating Users routes.');

    // get all technicians
    this.router.get('/', this.get);
  }

  private async get (req: Request, res: Response, next: NextFunction) {
    const data: any = decodeJWT(req.headers.authorization);
    User.find({
      is_active: true,
      type: 'technician',
      email_verified: true,
    }).select('email first_name last_name member_of email_verified is_active type created_at')
      .exec()

      .then((doc) => {

        if (_.isEqual(doc, null)) {

          res.status(200).jsonp({message: 'User Not found'});

        } else {

          res.status(200).jsonp({doc});

        }
      })

      .catch((err) => {

        logger.error(err);

        res.status(500).jsonp({error: err});

      });

  }
}
