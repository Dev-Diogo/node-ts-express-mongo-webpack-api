import { User } from '@/models';
import { decodeJWT, errorHandler, logger } from '@/services';
import { DeniedResponse, UserPermHandler } from '@/services/permissionsHandler';
import * as bcrypt from 'bcryptjs';
import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator/check';
import { BaseRoute } from '../../../route';
import _ = require('lodash');

export class UsersRoute extends BaseRoute {
  public static path = '/users';
  private static instance: UsersRoute;

  private constructor() {
    super();

    this.init();
    this.router.use(errorHandler);
  }

  static get router() {
    if (!UsersRoute.instance) {
      UsersRoute.instance = new UsersRoute();
    }
    return UsersRoute.instance.router;
  }

  private init() {

    // log
    logger.info('[UsersRoute] Creating Users routes.');

    // get all users
    this.router.get('/', this.get);
    // get user info
    this.router.get('/me', this.getMe);

    this.router.get('/:id', this.getById);

    // update user auth info
    this.router.patch('/', this.patch); // DONE

    // delete user
    this.router.delete('/', this.delete); // TODO

  }

  private async get(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'user:read', (result) => {
      if (!result) {
        DeniedResponse(res);
      } else {
        const data: any = decodeJWT(req.headers.authorization);

        User.find().select('email first_name last_name member_of email_verified is_active type created_at')
          .exec()
          .then((doc) => {

            if (_.isEqual(doc, null)) {

              res.status(200).jsonp({message: 'User Not found'});

            } else {

              res.status(200).jsonp({doc});

            }
          })

          .catch((err) => {

            logger.error(err);

            res.status(500).jsonp({error: err});

          });
      }

    });
  }

  private async getMe(req: Request, res: Response, next: NextFunction) {
    const data: any = decodeJWT(req.headers.authorization);
    User.findById(data._id).select('permissions email first_name last_name terms_agreed member_of email_verified is_active type created_at')
      .exec()

      .then((doc) => {

        if (_.isEqual(doc, null)) {

          res.status(200).jsonp({message: 'User Not found'});

        } else {

          res.status(200).jsonp({doc});

        }
      })

      .catch((err) => {

        logger.error(err);

        res.status(500).jsonp({error: err});

      });

  }

  private async getById(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'user:update', (result) => {
      if (!result) {
        DeniedResponse(res);
      }
      const data: any = decodeJWT(req.headers.authorization);
      User.findById(req.params.id).select('permissions email first_name last_name member_of email_verified is_active type created_at')
        .exec()

        .then((doc) => {

          if (_.isEqual(doc, null)) {

            res.status(200).jsonp({message: 'User Not found'});

          } else {

            res.status(200).jsonp({doc});

          }
        })

        .catch((err) => {

          logger.error(err);

          res.status(500).jsonp({error: err});

        });
    });

  }

  private patch(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'users:update', (result) => {
      const errors = validationResult(req);

      // check if parameters are missing
      if (!errors.isEmpty()) {

        return res.status(422).json({errors: errors.array()});

      }

      const token = req.headers.authorization;

      const {_id}: any = decodeJWT(token);

      let updateOps: any = {};

      for (const ops of req.body) {

        if (ops.propname === 'password') {

          updateOps[ops.propname] = bcrypt.hashSync(ops.value, 12);

        } else if (ops.propname === 'permissions') {

          res.status(500).jsonp({error: 'illegal modification'});

        } else {

          updateOps[ops.propname] = ops.value;

        }

      }

      logger.error('[temp ops] ' + updateOps.email);

      if (updateOps.email) {

        User.findOne({email: updateOps.email})

          .exec()

          .then((doc) => {

            if (doc) {

              // tslint:disable-next-line:triple-equals
              if (doc._id != _id) {

                res.status(500).jsonp({error: 'email already in Use!'});

              }
            }
            User.updateOne({_id}, {$set: updateOps})

              .exec()

              .then((result) => {

                const data: any = decodeJWT(req.headers.authorization);
                User.findById(data._id).select('permissions email first_name last_name terms_agreed member_of email_verified is_active type created_at')
                  .exec()

                  .then((doc) => {

                    if (_.isEqual(doc, null)) {

                      res.status(200).jsonp({message: 'User Not found'});

                    } else {

                      res.status(200).jsonp({doc});

                    }
                  });

              })

              .catch((err) => {

                logger.error('[Failed Update]' + err.toString());
                res.status(500).jsonp('User Not updated');

              });

          })
          .catch((err) => {

            logger.error('[email duplicate]' + err.toString());
            res.status(500).jsonp({errors: err});

          });
      } else {

        User.updateOne({_id}, {$set: updateOps})
          .exec()

          .then((result) => {

            const data: any = decodeJWT(req.headers.authorization);
            User.findById(data._id).select('permissions email first_name last_name terms_agreed member_of email_verified is_active type created_at')
              .exec()

              .then((doc) => {

                if (_.isEqual(doc, null)) {

                  res.status(200).jsonp({message: 'User Not found'});

                } else {

                  res.status(200).jsonp({doc});

                }
              });

          })

          .catch((err) => {

            logger.error('[Failed Update]' + err.toString());
            res.status(500).jsonp('User Not updated');

          });
      }

    });
  }

  private delete(req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'users:delete', (result) => {
      const token: any = decodeJWT(req.headers.authorization);

      User
        .findByIdAndDelete({_id: token._id})

        .exec()

        .then((result) => {

          res.status(200).jsonp({result});

        })

        .catch((err) => {

          logger.error('[User Delete]' + err);

          res.status(200).jsonp({err});

        });

    });
  }
}
