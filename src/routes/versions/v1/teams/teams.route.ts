import { Team } from '@/models';
import { decodeJWT, errorHandler, logger } from '@/services';
import { TeamPermHandler, UserPermHandler } from '@/services/permissionsHandler';
import * as cors from 'cors';
import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator/check';
import * as mongoose from 'mongoose';
import { BaseRoute } from '../../../route';

export class TeamsRoute extends BaseRoute {
  public static path = '/teams';
  private static instance: TeamsRoute;

  private constructor () {

    super();

    this.init();
    this.router.use(errorHandler);
    this.router.use(cors());

  }

  static get router () {

    if (!TeamsRoute.instance) {

      TeamsRoute.instance = new TeamsRoute();

    }

    return TeamsRoute.instance.router;
  }

  private init () {

    // log
    logger.info('[TeamsRoute] Creating Teams routes.');

    /* add index page route*/
    this.router.get('/', this.get); // DONE

    /* get team by id*/
    this.router.get('/:teamID', this.getById); // DONE

    this.router.post('/', this.post); // DONE

    /* patch tem by id*/
    this.router.patch('/:teamID', this.patch); // TODO

    /* delete by id*/
    this.router.delete('/:teamID', this.delete); // TODO

  }

  private async get (req: Request, res: Response, next: NextFunction) {

    UserPermHandler(req.headers.authorization, 'team:read', (result) => {

      if (!result) {

        res.status(403).jsonp({message: 'Access Denied. Not enough permissions'});

      } else {

        const token: any = decodeJWT(req.headers.authorization);

        Team.find({members: {$elemMatch: {member_id: token._id}}}).populate('members.member_id owner_id')
          .exec()

          .then((docs) => {

            res.status(200).jsonp(docs);

          })
          .catch((err) => {

            logger.error('[Teams Route GET]' + err);

          });

      }
    });

  }

  private async getById (req: Request, res: Response, next: NextFunction) {

    TeamPermHandler(req.headers.authorization, 'team:read', req.params.teamID, (result) => {

      if (!result) {

        res.status(403).jsonp({message: 'Access Denied. Not enough permissions'});

      } else {

        const token: any = decodeJWT(req.headers.authorization);

        Team.find({_id: req.params.teamID}).populate('members.member_id owner_id')

          .exec()

          .then((docs) => {

            res.status(200).jsonp(docs);

          })
          .catch((err) => {

            logger.error('[Teams Route GET]' + err);

          });

      }

    });
  }

  private async post (req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'team:create', (result) => {
      const errors = validationResult(req);

      // check if parameters are missing
      if (!errors.isEmpty()) {

        return res.status(422).json({errors: errors.array()});

      }

      const token: any = decodeJWT(req.headers.authorization);
      const {team_name} = req.body;
      const newTeam = new Team({

        _id: new mongoose.Types.ObjectId(),

        team_name,

        creation_date: new Date(),

        members: [{

          member_id: token._id,

          join_date: new Date(),

          team_permissions: [
            'team:create',
            'team:read',
            'team:update',
            'team:delete',
          ],
        }],

        owner_id: token._id,
      });

      newTeam
        .save()

        .then((result) => {

          res.status(200).jsonp(result);

        })
        .catch((err) => {

          logger.error('[Team Create]' + err);
          res.status(200).jsonp({message: err});

        });

    });
  }

  private async patch (req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'team:update', (result) => {
      let addOps: any = {};
      let removeOps: any = {};
      let updateOps: any = {};
      let results: any = {};
      const teamId = req.params.teamID;

      for (const ops of req.body) {

        if (ops.propaction === 'ADD') {

          addOps[ops.propname] = ops.value;

        } else if (ops.propaction === 'DELETE') {

          removeOps[ops.propname] = ops.value;

        } else if (ops.propaction === 'PATCH') {

          updateOps[ops.propname] = ops.value;

        }

        // res.status(200).jsonp({updateOps});

        Team.updateOne({_id: teamId}, {$set: updateOps})

          .exec()

          .then((result) => {
            results.PATCH = {

              status: 200,

              message: result,

            };
          })
          .catch((err) => {

            logger.error('[Failed Update]' + err.toString());
            results.PATCH = {

              status: 500,

              message: err,

            };
          });

        res.status(200).jsonp(results);
      }

    });
  }

  private async delete (req: Request, res: Response, next: NextFunction) {
    UserPermHandler(req.headers.authorization, 'team:delete', (result) => {

      res.json('Teams');

    });
  }

}
