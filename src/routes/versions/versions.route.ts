import { NextFunction, Request, Response } from 'express';
import { logger } from '../../services';
import { BaseRoute } from '../route';
import { V1Route } from './v1';

export class VersionsRoute extends BaseRoute {
  public static path = '/versions';
  private static instance: VersionsRoute;

  private constructor () {
    super();
    this.init();
  }

  static get router () {
    if (!VersionsRoute.instance) {
      VersionsRoute.instance = new VersionsRoute();
    }
    return VersionsRoute.instance.router;
  }

  private init () {
    // log
    logger.info('[VersionsRoute] Creating Versions routes.');

    // add index page route
    this.router.get('/', this.get);

    // add v1 routes
    this.router.use(V1Route.path, V1Route.router);
    // this.router.use(V2Route.path, V2Route.router);
  }

  private async get (req: Request, res: Response, next: NextFunction) {
    res.json('versions');
    next();
  }
}
