import { logger } from '@/services';
import { NextFunction, Request, Response } from 'express';
import { BaseRoute } from '../../route';

/**
 * @api {get} /ping Ping Request customer object
 * @apiName Ping
 * @apiGroup Ping
 *
 * @apiSuccess {String} type Json Type.
 */
export class V2Route extends BaseRoute {
  public static path = '/v2';
  private static instance: V2Route;

  /**
   * @class VersionsRoute
   * @constructor
   */
  private constructor () {
    super();
    this.get = this.get.bind(this);
    this.init();
  }

  static get router () {
    if (!V2Route.instance) {
      V2Route.instance = new V2Route();
    }
    return V2Route.instance.router;
  }

  private init () {
    // log
    logger.info('[V2Route] Creating ping routes.');

    // add index page route
    this.router.get('/', this.get);

  }

  /**
   * @class VersionsRoute
   * @method get
   * @param req {Request} The express Request object.
   * @param res {Response} The express Response object.
   * @param next {NextFunction} Execute the next method.
   */
  private async get (req: Request, res: Response, next: NextFunction) {
    res.json('v1');
    next();
  }
}
