import { Request, Response } from 'express';
import { logger } from '../services';
import { BaseRoute } from './route';
import { VersionsRoute } from './versions';

export class ApiRoutes extends BaseRoute {
  public static path = '/api';
  private static instance: ApiRoutes;

  private constructor() {
    super();

    this.init();
  }

  static get router() {
    if (!ApiRoutes.instance) {
      ApiRoutes.instance = new ApiRoutes();
    }
    return ApiRoutes.instance.router;
  }

  private init() {

    // log
    logger.info('[ApiRoute] Creating api routes.');

    // add index page route
    this.router.get('/', this.get);

    // add children versions route
    this.router.use(VersionsRoute.path, VersionsRoute.router);

  }

  private get(req: Request, res: Response) {

  }
}
